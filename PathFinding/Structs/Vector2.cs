﻿namespace PathFinding.Structs
{
    public struct Vector2
    {
        public int x;
        public int y;

        public Vector2(int x = 0, int y = 0)
        {
            this.x = x;
            this.y = y;
        }
        
        public static Vector2 operator +(Vector2 a, Vector2 b)
        {
            a.x += b.x;
            a.y += b.y;

            return a;
        }

        public override string ToString()
        {
            return $"({y}, {x})";
        }
    }
}